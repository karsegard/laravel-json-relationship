<?php

namespace VendorName\Tests\Behat\Context;


/**
 * Defines application features from the specific context.
 */
class PackageContext extends BaseContext
{
    use Concerns\Factory;
    use Concerns\Models;
}
